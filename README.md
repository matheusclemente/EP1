# EP1 - OO (UnB - Gama)
# Turma A - Profº Paulo
# Aluno: Matheus Clemente Carvalho de Azevedo - 15/0042817

Programa destinado a aplicação de filtro em imagens em formato .ppm

Para compilar e executar, siga as seguintes instruções:
* Abra o terminal
* Entre no diretório do projeto:
	**$ cd diretorio/**
* Limpe os arquivos *.o:
	**$ make clean**
* Compile o programa:
	**$ make**
* Execute:
	**$ make run**
