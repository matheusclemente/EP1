#ifndef FILTRO_HPP
#define FILTRO_HPP
#include "imagem.hpp"

using namespace std;

class Filtro{
protected:
  Imagem imagemFiltrar;
public:
  Filtro();
  Filtro(Imagem imagemFiltrar);
  //~Filtro();
  void setImagem(Imagem imagemFiltrar);
  Imagem getImagem();
  void aplicaFiltro();
};

#endif
