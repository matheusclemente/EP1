#ifndef IMAGEM_HPP
#define IMAGEM_HPP
#include <string>
#include <fstream>

using namespace std;

struct rgb{
  unsigned char r;
  unsigned char g;
  unsigned char b;
};

class Imagem {
private:
  string caracteresIdentificacao;
  int largura;
  int altura;
  int maximoCor;
  struct rgb ** pixel;

public:
  Imagem();
  //~Imagem();
  void setCaracteresIdentificacao(ifstream &arquivo);
  string getCaracteresIdentificacao();
  void setLargura(ifstream &arquivo);
  int getLargura();
  void setAltura(ifstream &arquivo);
  int getAltura();
  void setMaximoCor(ifstream &arquivo);
  int getMaximoCor();
  void alocaPixels(int largura, int altura);
  void setPixels(ifstream &arquivo);
  void setPixels(struct rgb **pixel);
  struct rgb getPixel(int altura, int largura);
  void lerImagem(ifstream &arquivo);
  void salvarArquivo(ofstream &arquivoNovo);
};

#endif
