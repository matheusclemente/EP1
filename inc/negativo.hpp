#ifndef NEGATIVO_HPP
#define NEGATIVO_HPP
#include "filtro.hpp"
#include "imagem.hpp"

class Negativo : public Filtro{
public:
  Negativo();
  Negativo(Imagem imagemFiltrar);
  void aplicaFiltro();
};

#endif
