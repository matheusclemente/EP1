#ifndef PRETOEPRANCO_HPP
#define PRETOEPRANCO_HPP
#include "filtro.hpp"
#include "imagem.hpp"

class PretoEBranco : public Filtro{
public:
  PretoEBranco();
  PretoEBranco(Imagem imagemFiltrar);
  void aplicaFiltro();
};

#endif
