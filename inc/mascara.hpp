#ifndef MASCARA_HPP
#define MASCARA_HPP
#include "filtro.hpp"
#include "imagem.hpp"

class Mascara : public Filtro{
public:
  Mascara();
  Mascara(Imagem imagemFiltrar);
  void aplicaFiltro();
};

#endif
