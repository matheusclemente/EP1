#ifndef POLARIZADO_HPP
#define POLARIZADO_HPP
#include "filtro.hpp"
#include "imagem.hpp"

class Polarizado : public Filtro{
public:
  Polarizado();
  Polarizado(Imagem imagemFiltrar);
  void aplicaFiltro();
};

#endif
