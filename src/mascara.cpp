#include "mascara.hpp"
#include "imagem.hpp"
#include <stdlib.h>
#include <iostream>

Mascara::Mascara(){

}
Mascara::Mascara(Imagem imagemFiltrar){
  this->imagemFiltrar = imagemFiltrar;
}
void Mascara::aplicaFiltro(){
  struct rgb ** novoPixel;
  int value_r, value_g, value_b;
  int i, j, x,y, limite, tamanho, opcao = 0;

  novoPixel = (struct rgb **)malloc(imagemFiltrar.getAltura() * sizeof(struct rgb *));
  for (i=0; i < imagemFiltrar.getAltura(); i++){
    novoPixel[i] = (struct rgb *)malloc(imagemFiltrar.getLargura() * sizeof(struct rgb));
  }

  cout << "Escolha o tamanho da máscara:"<< endl;
  cout << "(1) - 3x3:"<< endl;
  cout << "(2) - 5x5"<< endl;
  cout << "(3) - 7x7"<< endl;

  while (opcao < 1 || opcao > 3){
  cin >> opcao;
  switch (opcao) {
    case 1:
      tamanho = 3;
      break;
    case 2:
      tamanho = 5;
      break;
    case 3:
      tamanho = 7;
      break;
    default:
      cout << "Por favor, selecione uma opção válida:" << endl;
      opcao = 0;
      break;
    }
  }
limite = tamanho/2;
for(i = limite; i < imagemFiltrar.getAltura() - limite; ++i){
    for(j = limite; j < imagemFiltrar.getLargura() - limite; ++j) {
      value_r = 0;
      value_g = 0;
      value_b = 0;
           for(x=0-limite; x <tamanho-limite; x++) {
                    for(y=0-limite; y<tamanho - limite;y++){
                          value_r += imagemFiltrar.getPixel(i+x,j+y).r;
                          value_b += imagemFiltrar.getPixel(i+x,j+y).g;
                          value_g += imagemFiltrar.getPixel(i+x,j+y).b;
                    }
           }

           value_r /= (tamanho*tamanho);
           value_g /= (tamanho*tamanho);
           value_b /= (tamanho*tamanho);

           novoPixel[i][j].r = value_r;
           novoPixel[i][j].g = value_g;
           novoPixel[i][j].b = value_b;
         }
}
imagemFiltrar.setPixels(novoPixel);
}
