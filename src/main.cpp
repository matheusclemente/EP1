#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "imagem.hpp"
#include "filtro.hpp"
#include "negativo.hpp"
#include "polarizado.hpp"
#include "pretoebranco.hpp"
#include "mascara.hpp"

using namespace std;

int main() {
  int opcao=0;
  char nome_imagem[20], nome_nova_imagem[50];
  Negativo negativo1;
  Polarizado polarizado1;
  PretoEBranco peb1;
  Mascara masc1;
  Imagem imagem1;
  ifstream ppm;
  ofstream ppm_copia;

  cout << "Digite o endereco e nome da imagem a ser lida" << endl;
  cin >> nome_imagem;

  ppm.open(nome_imagem, ifstream::binary);

  if (!ppm){
    cout << "A imagem nao pode ser aberta. Por favor, verifique o endereço"<< endl;
    return -1;
  }

  imagem1.lerImagem(ppm);

  if (imagem1.getCaracteresIdentificacao() != "P6"){
    cout<<"A imagem não esta no formato correto"<< endl;
    ppm.close();
    return -1;
  }

  ppm.close();

  cout << "Escolha a opção de Filtro:" << endl;
  cout << "(1) - Negativo" << endl;
  cout << "(2) - Polarizado" << endl;
  cout << "(3) - Preto e Branco" << endl;
  cout << "(4) - Média" << endl;

while(opcao < 1 || opcao > 4){
  cin >> opcao;
  switch (opcao) {
    case 1:
      negativo1.setImagem(imagem1);
      negativo1.aplicaFiltro();
      imagem1 = negativo1.getImagem();
      break;
    case 2:
      polarizado1.setImagem(imagem1);
      polarizado1.aplicaFiltro();
      imagem1 = polarizado1.getImagem();
      break;
    case 3:
      peb1.setImagem(imagem1);
      peb1.aplicaFiltro();
      imagem1 = peb1.getImagem();
      break;
    case 4:
      masc1.setImagem(imagem1);
      masc1.aplicaFiltro();
      imagem1 = masc1.getImagem();
      break;
    default:
      cout << "Por favor, selecione uma opcao válida" << endl;
      break;
  }
}

cout << "Digite o nome da nova imagem: ";
cin >> nome_nova_imagem;

ppm_copia.open(nome_nova_imagem);

imagem1.salvarArquivo(ppm_copia);

ppm_copia.close();

cout << "Imagem salva com sucesso!" << endl;

  return 0;
}
