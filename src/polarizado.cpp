#include "polarizado.hpp"
#include "imagem.hpp"
#include <stdlib.h>

Polarizado::Polarizado(){

}
Polarizado::Polarizado(Imagem imagemFiltrar){
  this->imagemFiltrar = imagemFiltrar;
}

void Polarizado::aplicaFiltro(){
  struct rgb ** novoPixel;
  int i, j;

  novoPixel = (struct rgb **)malloc(imagemFiltrar.getAltura() * sizeof(struct rgb *));
  for (i=0; i < imagemFiltrar.getAltura(); i++){
    novoPixel[i] = (struct rgb *)malloc(imagemFiltrar.getLargura() * sizeof(struct rgb));
  }

  for(i = 0; i < imagemFiltrar.getAltura(); ++i){
  for(j = 0; j < imagemFiltrar.getLargura(); ++j){
         if(imagemFiltrar.getPixel(i,j).r < imagemFiltrar.getMaximoCor()/2){
             novoPixel[i][j].r =(unsigned char) 0;
         }else{
             novoPixel[i][j].r = (unsigned char)imagemFiltrar.getMaximoCor();
         }

         if(imagemFiltrar.getPixel(i,j).g < imagemFiltrar.getMaximoCor()/2){
             novoPixel[i][j].g = (unsigned char)0;
         }else{
             novoPixel[i][j].g = (unsigned char)imagemFiltrar.getMaximoCor();
         }

         if(imagemFiltrar.getPixel(i,j).b < imagemFiltrar.getMaximoCor()/2){
             novoPixel[i][j].b = (unsigned char)0;
         }else{
             novoPixel[i][j].b = (unsigned char)imagemFiltrar.getMaximoCor();
         }
  }
}

imagemFiltrar.setPixels(novoPixel);

}
