#include "negativo.hpp"
#include "imagem.hpp"
#include <stdlib.h>

Negativo::Negativo(){

}

Negativo::Negativo(Imagem imagemFiltrar){
  this->imagemFiltrar = imagemFiltrar;
}

void Negativo::aplicaFiltro(){
  struct rgb ** novoPixel;
  int i, j;

  novoPixel = (struct rgb **)malloc(imagemFiltrar.getAltura() * sizeof(struct rgb *));
  for (i=0; i < imagemFiltrar.getAltura(); i++){
    novoPixel[i] = (struct rgb *)malloc(imagemFiltrar.getLargura() * sizeof(struct rgb));
  }

  for(i = 0; i < imagemFiltrar.getAltura(); i++){
    for(j = 0; j < imagemFiltrar.getLargura(); j++){
        novoPixel[i][j].r = (unsigned char)255 - imagemFiltrar.getPixel(i,j).r;
        novoPixel[i][j].g = (unsigned char)255 - imagemFiltrar.getPixel(i,j).g;
        novoPixel[i][j].b = (unsigned char)255 - imagemFiltrar.getPixel(i,j).b;
    }
  }
imagemFiltrar.setPixels(novoPixel);
}
