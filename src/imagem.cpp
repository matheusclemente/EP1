#include "imagem.hpp"
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>

using namespace std;

Imagem::Imagem(){
}

void Imagem::setCaracteresIdentificacao(ifstream &arquivo){
  char c;
  arquivo >> c;
  while (c == '#'){
    arquivo.ignore(1000, '\n');
  }
  arquivo.unget();

  arquivo >> caracteresIdentificacao;
}

string Imagem::getCaracteresIdentificacao(){
  return caracteresIdentificacao;
}

void Imagem::setLargura(ifstream &arquivo){
  char c;
  arquivo >> c;

  while (c == '#'){
    arquivo.ignore(1000, '\n');

    arquivo >> c;
  }
  arquivo.unget();
  arquivo >> largura;
}

int Imagem::getLargura(){
  return largura;
}

void Imagem::setAltura(ifstream &arquivo){
  arquivo >> altura;
}
int Imagem::getAltura(){
  return altura;
}
void Imagem::setMaximoCor(ifstream &arquivo){
  char c;
  arquivo >> c;

  while (c == '#'){
    arquivo.ignore(1000, '\n');
    arquivo >> c;
  }
  arquivo.unget();
  arquivo >> maximoCor;
  arquivo.get();
}
int Imagem::getMaximoCor(){
  return maximoCor;
}

void Imagem::alocaPixels(int largura, int altura){
  int i;
  pixel = (struct rgb **)malloc(altura * sizeof(struct rgb *));
  for (i=0; i < altura; i++){
    pixel[i] = (struct rgb *)malloc(largura * sizeof(struct rgb));
  }
}

void Imagem::setPixels(ifstream &arquivo){
  int i,j;
  for(i=0; i<altura; i++){
    for(j=0; j<largura; j++){
      pixel[i][j].r = arquivo.get();
      pixel[i][j].g = arquivo.get();
      pixel[i][j].b = arquivo.get();
}
}
}

void Imagem::setPixels(struct rgb ** pixel){
  this->pixel = pixel;
}

struct rgb Imagem::getPixel(int altura, int largura){
  return pixel[altura][largura];
}

void Imagem::lerImagem(ifstream &arquivo){
  setCaracteresIdentificacao(arquivo);
  setLargura(arquivo);
  setAltura(arquivo);
  setMaximoCor(arquivo);
  alocaPixels(getLargura(), getAltura());
  setPixels(arquivo);
}

void Imagem::salvarArquivo(ofstream &arquivoNovo){
  arquivoNovo << caracteresIdentificacao << endl;
  arquivoNovo << largura <<" "<< altura << endl;
  arquivoNovo << maximoCor << endl ;

  int i,j;
  for(i=0; i<altura; i++){
    for(j=0; j<largura; j++)
    arquivoNovo << pixel[i][j].r << pixel[i][j].g << pixel[i][j].b;
  }
}
