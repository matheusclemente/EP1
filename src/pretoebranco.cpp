#include "pretoebranco.hpp"
#include "imagem.hpp"
#include <stdlib.h>

PretoEBranco::PretoEBranco(){
}
PretoEBranco::PretoEBranco(Imagem imagemFiltrar){
  this->imagemFiltrar = imagemFiltrar;
}
void PretoEBranco::aplicaFiltro(){
  struct rgb ** novoPixel;
  int i, j, grayscale_value;

  novoPixel = (struct rgb **)malloc(imagemFiltrar.getAltura() * sizeof(struct rgb *));
  for (i=0; i < imagemFiltrar.getAltura(); i++){
    novoPixel[i] = (struct rgb *)malloc(imagemFiltrar.getLargura() * sizeof(struct rgb));
  }

  for(i = 0; i < imagemFiltrar.getAltura(); ++i){
    for(j = 0; j < imagemFiltrar.getLargura() ; ++j){
           grayscale_value = (0.299 * imagemFiltrar.getPixel(i,j).r) + (0.587 * imagemFiltrar.getPixel(i,j).g) + (0.144 * imagemFiltrar.getPixel(i,j).b);

           if(grayscale_value > imagemFiltrar.getMaximoCor())
            grayscale_value = imagemFiltrar.getMaximoCor();

           novoPixel[i][j].r = grayscale_value;
           novoPixel[i][j].g = grayscale_value;
           novoPixel[i][j].b = grayscale_value;
    }
}
imagemFiltrar.setPixels(novoPixel);

}
